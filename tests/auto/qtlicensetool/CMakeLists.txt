# Copyright (C) 2023 The Qt Company Ltd.
#
# Published under GPL-3.0 with Qt-GPL-exception-1.0
#

include_directories(${CMAKE_CURRENT_LIST_DIR}/../../../src/qtlicensetool/)

list(APPEND extSrc ${CMAKE_CURRENT_LIST_DIR}/../../../src/qtlicensetool/qtlicensetool.cpp)

add_executable(tst_qtlicensetool ${extSrc} tst_qtlicensetool.cpp)
target_link_libraries(tst_qtlicensetool PRIVATE Catch2 qlicenseservice)

add_test(NAME tst_qtlicensetool COMMAND tst_qtlicensetool)
