/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include <catch_amalgamated.hpp>

#include "commonsetup.h"

#include "qtlicensetool.h"
#include "licdsetup.h"
#include "licenseservice.h"

using namespace QLicenseService;

TEST_CASE("Version query", "[qtlicensetool]")
{
    LicenseService service(0, DAEMON_SETTINGS_FILE_DEFAULT);
    LicdSetup setup(e_set_type_licensetool, LicdSetup::getQtAppDataLocation() + USER_SETTINGS_FILE);

    int result = setup.initSettings();
    if (result == e_file_not_present) {
        // initSettings() creates the file if it does not exist, just call it again to reinit
        result = setup.initSettings();
    }

    // 1. Verify settings initialization for license tool
    REQUIRE(result == e_settings_ok);
    // 2. Verify starting of license "service" succeeds
    REQUIRE((service.start() && service.waitForStarted()));
    // 3. Verify send version command & reply status ok
    REQUIRE(askStatus(DAEMON_VERSION_CMD, &setup) == 0);
    // 4. Verify stop service
    REQUIRE((service.cancel() && service.waitForFinished()));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}
