/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include <catch_amalgamated.hpp>

#include "commonsetup.h"

#include "licenseservice.h"

using namespace QLicenseService;

TEST_CASE("Start/stop without clients", "[licenseservice]")
{
    LicenseService service(0, DAEMON_SETTINGS_FILE_DEFAULT);
    // 1. Verify starting of license "service" succeeds
    REQUIRE((service.start() && service.waitForStarted()));
    // 2. Verify stop service
    REQUIRE((service.cancel() && service.waitForFinished()));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}

TEST_CASE("Start two daemon instances with same port", "[licenseservice]")
{
    LicenseService service(0, DAEMON_SETTINGS_FILE_DEFAULT);
    // 1. Verify starting of license "service" succeeds
    REQUIRE((service.start() && service.waitForStarted()));

    LicenseService service2(0, DAEMON_SETTINGS_FILE_DEFAULT);
    // 2. Verify starting another instance fails gracefully
    REQUIRE((service2.start() && !service2.waitForStarted()));
    REQUIRE(service2.status() == LicenseService::Status::Error);

    // 3. Verify stop original service
    REQUIRE((service.cancel() && service.waitForFinished()));
    REQUIRE(service.status() == LicenseService::Status::Canceled);
}
