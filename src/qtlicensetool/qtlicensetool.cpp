/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "qtlicensetool.h"

#include "commonsetup.h"

#include "tcpclient.h"
#include "utils.h"
#include "version.h"

using namespace QLicenseService;

void showVersion()
{
    std::cout << "qtlicensetool v" << QTLICENSETOOL_VERSION
                << " - CLI tool for Qt Licensing\n";
    return;
}

int askStatus(const std::string &statusRequest, LicdSetup *setup)
{
    std::string req = statusRequest + " -a ";
    req += QTLICENSETOOL_APP_NAME;
    req += " -v ";
    req += QTLICENSETOOL_VERSION;
    std::string daemonAddr = setup->get("licd_addr");
    int daemonPort = utils::strToInt(setup->get("licd_port"));
    if (daemonPort < 0) {
        errorAndExit("Invalid daemon port - check settings/arguments");
    }

    // Connect and send/receive the request
    TcpClient tcp(daemonAddr, daemonPort);
    std::string reply;
    int result = tcp.sendAndReceive(req, reply);
    if (result != e_tcp_success) {
        std::cout << tcp.errorString(result) << std::endl;
        return e_tcp_error_conn;
    }
    std::cout << reply << std::endl;
    return 0;
}

void overrideSetup(LicdSetup *setup, int argc, char *argv[])
{
    // Pick up the cmd-line switches: Start looping args from index 1,
    // as first one is the command itself
    bool permanent = false;
    for (int i = 1; i < argc; i++) {
        std::string sw = utils::trimStr(argv[i]);
        std::string val = "";
        if (sw == "-p" || sw == "--permanent") {
            permanent = true;
            continue;
        }
        if (permanent) {
            // Permanent operation (add|remove) has to come right after the -P switch, not later
            if (sw == OP_ADD_RESERVATION || sw == OP_REMOVE_RESERVATION) {
                setup->set("operation", sw);
                permanent = false;
                continue;
            }
            else {
                errorAndExit("Invalid operation for permanen license request");
            }
        }

        // Check if given switch has a value
        if (sw[0] == '-' && argc > i+1) {
            val = utils::trimStr(argv[i+1]);
            if (val[0] == '-') { // do not accept another switch as a value
                val = "";
            } else {
                i++;
            }
        }
        if (val.empty()) {
            std::string error = "No value for argument " + sw;
            errorAndExit(error);
        }
        // Pick up values
        if (sw == "-i") {
            setup->set("license_id", val);
        } else if (sw == "-u") {
            setup->set("user_id", val);
        } else if (sw == "-d") {
            size_t colPos = val.find(':');
            if (colPos == std::string::npos) {
                errorAndExit("Invalid service address: Must be in form <url>:<port>");
            }
            setup->set("licd_addr", val.substr(0, colPos));
            setup->set("licd_port", val.substr(colPos+1));
        } else {
            std::string error = "Invalid argument:" + sw;
            errorAndExit(error);
        }
    }
}

int doPermanentRequest(LicdSetup *setup)
{
    std::string operation = setup->get("operation");
    if (operation != OP_ADD_RESERVATION && operation != OP_REMOVE_RESERVATION) {
        std::string error = "Invalid operation \"" + operation + "\"";
        error += "\"";
            errorAndExit(error);
    }
    int daemonPort = utils::strToInt(setup->get("licd_port"));
    if (daemonPort < 0) {
        errorAndExit("Invalid daemon port - check settings/arguments");
    }

    // Ready to build the request
    std::stringstream request;
    request << PERMANENT_REQUEST_CMD << " " << setup->get("operation")
            << " -u " << setup->get("user_id")
            << " -i " << setup->get("license_id")
            << " -e " << setup->get("user_email")
            << " -a " << QTLICENSETOOL_APP_NAME
            << " -v " << QTLICENSETOOL_VERSION;

    // Connect and send/receive the request:
    TcpClient tcp(setup->get("licd_addr"), daemonPort);
    std::string reply;
    int result = tcp.sendAndReceive(request.str(), reply);
    if (result != e_tcp_success) {
        std::cout << tcp.errorString(result) << std::endl;
        exit(EXIT_FAILURE);
    }
    if (reply.find("No") != std::string::npos
            || reply.find("ERROR") != std::string::npos
            || reply.find("expired") != std::string::npos) {
        errorAndExit(reply);
    }
    std::cout << reply << std::endl;
    exit(EXIT_SUCCESS);
}

void errorAndExit(const std::string &reason)
{
    if (!reason.empty()) {
        std::cout << "ERROR! " << reason << std::endl;
    }
    exit(EXIT_FAILURE);
}

void helpAndExit()
{
    std::cout << "Usage:\n"
        << "     qtlicensetool <Action> [params]\n"
        << "   Where 'Action' is:\n"
        << "     -h or --help                : Prints out the tool's help text\n"
        << "     -v or --version             : Prints out qtlicensetool version\n"
        << "     -S or --serverversion       : Prints out License Server version\n"
        << "     -D or --daemonversion       : Prints out the License Service (daemon) version\n"
        << "     -r or --reservation         : Prints out the list of active reservations\n"
        << "     -p or --permanent           : Request permanent reservation (add or remove)\n"
        << "   For permanent request ('-P' or '--peramanent'), you must tell which operation to do:\n"
        << "     add|remove  <--- Either one needs to be there\n"
        << "   Optionat parameters which override [default| section settings:\n"
        << "     -d = Full license daemon address and port separated by ':'\n"
        << "     -u = Your Qt license username\n"
        << "     -i = Your Qt license ID number (must be an int)\n"
        << "     -l = License server address and port  separated by ':'\n"
        << "   Example:\n"
        << "     > qtlicensetool --permanent add         : Make a permanent license reservation\n"
        << "     > qtlicensetool -P add -i 462412        : Same as above, but using different license ID\n"
        << "     > qtlicensetool --daemonversion         : Prints out a daemon version number\n";
    exit(EXIT_SUCCESS);
}
