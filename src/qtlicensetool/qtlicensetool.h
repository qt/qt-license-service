/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <cstring>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#if __APPLE__ || __MACH__ || __linux__
    #include <sys/types.h>
    #include <netdb.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
#else
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>
    // Need to link with Ws2_32.lib
    #pragma comment (lib, "Ws2_32.lib")

    typedef size_t ssize_t;
#endif

enum qt_tool_action_type {
    e_action_tool_version   = 1,
    e_action_server_version = 2,
    e_action_longterm       = 3,
    e_action_license_query  = 4
};

#include "licdsetup.h"

#define APP_NAME "Qt licensing CLI"
#define BUFFER_SIZE 1024

void showVersion();
int askStatus(const std::string &statusRequest, QLicenseService::LicdSetup *setup);
int doPermanentRequest(QLicenseService::LicdSetup *setup);
void overrideSetup(QLicenseService::LicdSetup *setup, int argc, char *argv[]);
void errorAndExit(const std::string &reason = "");
void helpAndExit();
