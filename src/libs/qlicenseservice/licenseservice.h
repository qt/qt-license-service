/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include "asynctask.h"

namespace QLicenseService {

class LicenseService : public AsyncTask<void>
{
public:
    explicit LicenseService(uint16_t tcpPort = 0, const std::string &settingsPath = "");

protected:
    void run() override;

private:
    uint16_t m_tcpPort = 0;
    std::string m_settingsPath;
};

} // namespace QLicenseService
