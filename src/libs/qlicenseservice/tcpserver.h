/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#pragma once

#include <iostream>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include <unordered_set>


#define BUFFER_SIZE 1024
#define TRUE 1
#define FALSE 0
#define SOCKET_CLOSED_MSG  "closed"
#define SOCKET_MAX_REACHED "clients_maxed_out"


#if __APPLE__ || __MACH__ || __linux__
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <netdb.h>
    #include <sys/time.h>
    typedef int type_socket;
#else
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>
    // Need to link with Ws2_32.lib
    #pragma comment (lib, "Ws2_32.lib")
    typedef SOCKET type_socket;
#endif


namespace QLicenseService {

class TcpServer
{
public:
    explicit TcpServer(uint16_t serverPort);
    ~TcpServer();

    bool init();
    bool listenToClients(uint16_t &socketId, std::string *data);
    int sendResponse(int socketIndex, const std::string &message);
    void closeClientSocket(int socketIndex);

private:
    uint16_t m_serverPort;
    type_socket m_masterSocket;
    std::unordered_set<type_socket> m_clientSocket;
    int m_addrlen;
    char m_buffer[BUFFER_SIZE]; // data buffer
    fd_set m_readfds; // set of socket descriptors
    struct sockaddr_in m_address;

    void doCloseSocket(type_socket &socketFD);
    bool generateSocketId(uint16_t &index);
};

} // namespace QLicenseService
