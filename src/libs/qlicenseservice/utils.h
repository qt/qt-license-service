/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include <sys/stat.h>
#include <sys/types.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstring>


#if _WIN32
    // Windows
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <stdint.h> // portable: uint64_t   MSVC: __int64
    #include <io.h>
    #include <iomanip>
    #include "../3rdparty/dirent/dirent.h"
    #define USER_SETTINGS_DIR  "AppData\\Roaming\\Qt"// Windows
    #define DIR_SEPARATOR '\\'
    #define SYSTEM_ROOT "C:"
#else
    // Linux or Mac
    #include <sys/time.h>
    #include <unistd.h>
    #include <dirent.h>
    #include <pwd.h>
    #include <grp.h>
    #define DIR_SEPARATOR '/'
    #define SYSTEM_ROOT "/"
#endif
#if __APPLE__ || __MACH__
    #define USER_SETTINGS_DIR  ".local/share/Qt/" // not supported yet
#elif __linux__
    #define USER_SETTINGS_DIR  ".local/share/Qt/"
#endif

namespace QLicenseService { namespace utils {
/*
*   String tools
*/
void trimLeft(std::string &s);
void trimRight(std::string &s);
std::string trimStr(const std::string &s);
std::vector<std::string> splitStr(const std::string &str, const char delimiter=' ');
std::string strToLower(const std::string &str);
std::string strToUpper(const std::string &str);
int strToInt(const std::string &str);
/*
*   Filesystem tools
*/
std::string getUserHomeDir();
int writeToFile(const std::string &filepath, const std::string &data, bool append=false);
int createDir(const std::string &filepath, uint16_t user=0, uint16_t group=0);
int readFile(std::string &str, const std::string &filepath);
bool fileExists(const std::string &name);
std::string getFileOwnerName(const std::string &filename);
int getFileOwner(const std::string &filename, uint16_t &owner, uint16_t &group);
std::vector<std::string> getDirListing(const std::string &directory, const std::string &filter = "");
int deleteFile(const std::string &filepath);
/*
*   Other
*/

// Hashing
void doHmacHashSha256(const std::string &payload, const std::string &secret, std::string &authKey);

// Time utils
std::string epochToString(time_t epochTime, const char* format = "%Y-%m-%d %H:%M:%S");
time_t stringToEpoch(const char* theTime, const char* format = "%Y-%m-%d %H:%M:%S");
uint64_t getTimestampNow();

// System utils
std::string getOsName();
std::string getSystemHwInfoString();

// Swap endian of any data
template <typename T> T swapEndian(T val)
{
    T retVal;
    char *pVal = (char*) &val;
    char *pRetVal = (char*)&retVal;
    int size = sizeof(T);
    for (int i=0; i<size; i++) {
        pRetVal[size-1-i] = pVal[i];
    }
    return retVal;
}
/*
*   App-specific utils
*/


} } // namespace QLicenseService::utils
