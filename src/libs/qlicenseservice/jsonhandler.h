/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include <algorithm>
#include <utility>
#include <map>
#include <vector>
#include <iostream>
#include <iomanip>

namespace QLicenseService {

// Very simple JSON reader/writer, takes a JSON string as input
// Specific to Qt license daemon only, doesn't work with JSONs in general
class JsonHandler  {

const std::vector<std::string> reservation {
    "reservation_id", "license_number", "email", "user_id",
    "hardware_id", "expiry_date", "license_key"
};

public:
    explicit JsonHandler(const std::string &jsonString);
    ~JsonHandler() {  }

    std::string get(const std::string &item);
    // Just in case type-specific are needed - here is some
    int getInt(const std::string &item);
    std::string getStr(const std::string &item);
    bool getBool(const std::string &item);

    void add(const std::string &item, const std::string &value);
    void add(const std::string &item, uint64_t value);
    void add(const std::string &item, bool value);

    std::string dump(uint8_t indent=0);

private:
    std::map<std::string, std::string> m_items;
    int preParse(const std::string &jsonString);

};

} // namespace QLicenseService
