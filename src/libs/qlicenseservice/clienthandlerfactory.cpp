/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "clienthandlerfactory.h"

#include "commonsetup.h"

#include "clitoolhandler.h"
#include "cocohandler.h"
#include "mochandler.h"
#include "pluginhandler.h"
#include "squishhandler.h"
#include "squishidehandler.h"

namespace QLicenseService {

ClientHandlerFactory &ClientHandlerFactory::instance()
{
    static ClientHandlerFactory factory;
    return factory;
}

ClientHandlerFactory::ClientHandlerFactory()
{
    registerHandler<CliToolHandler>(QTLICENSETOOL_APP_NAME);
    registerHandler<CocoHandler>(COCO_APP_NAME);
    registerHandler<MocHandler>(MOCWRAPPER_APP_NAME);
    registerHandler<PluginHandler>(CREATOR_APPNAME);
    registerHandler<PluginHandler>(DESIGN_STUDIO_APP_NAME);
    registerHandler<SquishHandler>(SQUISH_APP_NAME);
    registerHandler<SquishIdeHandler>(SQUISH_IDE_APP_NAME);
}

} // namespace QLicenseService
