/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/

#include "licenseservice.h"

#include "licenser.h"
#include "errors.h"

#if __linux__ || __APPLE__ || __MACH__
#include <csignal>
#endif

namespace QLicenseService {

LicenseService::LicenseService(uint16_t tcpPort, const std::string &settingsPath)
    : AsyncTask<void>()
    , m_tcpPort(tcpPort)
    , m_settingsPath(settingsPath)
{
    m_properties |= Properties::Cancelable;
}

void LicenseService::run()
{
#if __linux__ || __APPLE__ || __MACH__
        // block all signals in this thread - main thread handles
        // TODO: reconsider the signal handling pattern if we decide to create
        // separate worker threads for the TCP server and HTTP client
        sigset_t signal_set;
        sigfillset(&signal_set);
        pthread_sigmask(SIG_BLOCK, &signal_set, NULL);
#endif
        try {
            Licenser licenser(m_tcpPort, m_settingsPath);
            while (1) {
                licenser.listen();
                if (isCancelRequested()) {
                    updateAndNotifyStatus(Status::Canceled);
                    return;
                } else if (status() != Status::Running) {
                    updateAndNotifyStatus(Status::Running);
                }
            }
        } catch (const QLicenseService::Error &e) {
            setError("Caught exception: " + std::string(e.what()));
            updateAndNotifyStatus(Status::Error);
            return;
        } catch (const std::exception &e) {
            setError("Caught exception: " + std::string(e.what()));
            updateAndNotifyStatus(Status::Error);
            return;
        } catch (...) {
            setError("Caught unknown exception");
            updateAndNotifyStatus(Status::Error);
            return;
        }

        // Never reached
        updateAndNotifyStatus(Status::Finished);
}

} // namespace QLicenseService
