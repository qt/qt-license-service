/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "clienthandler.h"
#include "utils.h"

namespace QLicenseService {

class CocoHandler : public ClientHandler {

    public:
        CocoHandler(const RequestInfo &request, const LicdSetup &settings)
                : ClientHandler(request, settings)
        {
            const uint64_t updateInterval = utils::strToInt(m_settings.get("coco_report_interval"));
            m_updateInterval = updateInterval * SECS_IN_HOUR;
            m_request.updateIntervalSecs = updateInterval;

            m_floatingLicense = true;
            std::cout << "Client: Coco\n";
        }

        bool isLicenseRequestDue() override { return true; }
        bool isCachedReservationValid(std::string &reply) override {return true;}
        int parseAndSaveResponse(std::string &response) override { return 0; }
        void buildRequestJson() override {return;}
        void prepareRelease() override
        {
            // TODO
            return;
        };

};

} // namespace QLicenseService
