/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "clienthandler.h"
#include "jsonhandler.h"
#include "utils.h"

namespace QLicenseService {

class PluginHandler : public ClientHandler {

    public:
        PluginHandler(const RequestInfo &request, const LicdSetup &settings)
                : ClientHandler(request, settings) {
            std::cout << "Client: Plugin\n";
        }

        bool isLicenseRequestDue() override { return true; } // Pluging does timed requests, so every request counts

        void buildRequestJson() override
        {
            std::stringstream pay;
            pay << "{";
            pay<< "\"license_number\":" << "\"" << m_request.licenseId << "\",";
            pay << "\"user_id\":" << "\"" << m_request.userId << "\",";
            pay << "\"hw_id\":" << "\"" << m_settings.get("hw_id") << "\",";
            pay << "\"qt5_license_key\":" << "\"" << m_settings.get("license_key") << "\",";
            pay << "\"src\":" << "\"" << m_request.appName << "\",";
            pay << "\"host_os\":" << "\"" << m_settings.get("host_os") << "\",";
            pay << "\"src_version\":" << "\"" << m_request.appVersion << "\",";
            pay << "\"email\":" << "\"" << m_request.email << "\"";
            pay << "}";
            m_request.payload = pay.str();
        }

        bool isCachedReservationValid(std::string &reply) override
        {
            if (!checkLicenseExpiryTime(reply)) {
                if (checkLeewayTime(reply)) {
                    return true;
                }
            } else {
                reply = replyString[e_license_granted];
                return true;
            }
            return false;
        }

        int parseAndSaveResponse(std::string &response) override
        {
            JsonHandler json(response);
            // response JSON is converted to a reply string to the client from now on
            std::stringstream ss;

            if (json.get("status") != "true") {
                if (json.get("message") == "License fully reserved") {
                    response = replyString[e_license_pool_full];
                } else {
                    response = replyString[e_license_rejected];
                }
                // Remove the license file and return
                utils::deleteFile(m_request.licenseFile);
                return 1;
            }

            // License granted
            ss << replyString[e_license_granted];
            ss << " expiry_date=" << json.get("expiry_date");
            ss << " license_id=" << json.get("license_number");
            ss << " reservation_id=" << json.get("reservation_id");
            response = ss.str();

            // Save the license JSON
            int result = utils::writeToFile(m_request.licenseFile, json.dump(4));
            if (result != 0) {
                std::cout << "ERROR saving license file: '" << m_request.licenseFile << "': " << strerror(result) << std::endl;
            }
            return 0;
        }
};

} // namespace QLicenseService
