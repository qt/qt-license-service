/* Copyright (C) 2023 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "clienthandler.h"
#include "jsonhandler.h"
#include "utils.h"

namespace QLicenseService {

class MocHandler : virtual public ClientHandler {
    public:
        MocHandler(const RequestInfo &request, const LicdSetup &settings)
                : ClientHandler(request, settings)
        {
            m_updateInterval = utils::strToInt(m_settings.get("moc_renewal_interval")) * SECS_IN_HOUR;
            std::cout << "Client: MOC\n";
        }

        bool isCachedReservationValid(std::string &reply) override
        {
            if (!checkLicenseExpiryTime(reply)) {
                if (checkLeewayTime(reply)) {
                    return true;
                }
            } else {
                reply = replyString[e_license_granted];
                return true;
            }
            return false;
        }

        void buildRequestJson() override
        {
            std::stringstream pay;
            pay << "{";
            pay<< "\"license_number\":" << "\"" << m_request.licenseId << "\",";
            pay << "\"user_id\":" << "\"" << m_request.userId << "\",";
            pay << "\"hw_id\":" << "\"" << m_settings.get("hw_id") << "\",";
            pay << "\"qt5_license_key\":" << "\"" << m_settings.get("license_key") << "\",";
            pay << "\"src\":" << "\"" << m_request.appName << "\",";
            pay << "\"host_os\":" << "\"" << m_settings.get("host_os") << "\",";
            pay << "\"src_version\":" << "\"" << m_request.appVersion << "\",";
            pay << "\"email\":" << "\"" << m_request.email << "\"";
            pay << "}";
            m_request.payload = pay.str();
        }

        bool isLicenseRequestDue() override
        {
            std::cout << "MOC calling, checking if renewal is needed\n";
            // Open the license file
            std::string data;
            if (utils::readFile(data, m_request.licenseFile) != 0) {
                // If there is no license file yet, have to request anyway
                std::cout << "No license present (" << m_request.licenseFile << ") - requesting license\n";
                return true;
            }
            JsonHandler license(data);
            if (license.get("status") != "true") {
                // Cached license not valid
                utils::deleteFile(m_request.licenseFile);
                return true;
            }
            uint64_t timeNow = utils::getTimestampNow();
            if (timeNow > utils::strToInt(license.get("last_timestamp")) + m_updateInterval) {
                // Renewal is due
                std::cout << "Requesting license renewal\n";
                return true;
            }
            std::cout << "Renewal time not due, granting license\n";

            return false;
        }

        int parseAndSaveResponse(std::string &response) override
        {
            JsonHandler json(response);
            // response JSON is converted to a reply string to the client from now on
            std::stringstream ss;

            if (json.get("status") != "true") {
                if (json.get("message") == "License fully reserved") {
                    response = replyString[e_license_pool_full];
                } else {
                    response = replyString[e_license_rejected];
                }
                // Remove the license file and return
                utils::deleteFile(m_request.licenseFile);
                return 1;
            }

            ss << replyString[e_license_granted];
            ss << " expiry_date=" << json.get("expiry_date");
            ss << " license_id=" << json.get("license_number");
            ss << " reservation_id=" << json.get("reservation_id");
            response = ss.str();

            // Add timestamp into license JSON
            json.add("last_timestamp", utils::getTimestampNow());

            // Save the license JSON
            int result = utils::writeToFile(m_request.licenseFile, json.dump(4));
            if (result != 0) {
                std::cout << "ERROR saving license file: '" << m_request.licenseFile << "': " << strerror(result) << std::endl;
            }
            return 0;
        }
};

} // namespace QLicenseService
