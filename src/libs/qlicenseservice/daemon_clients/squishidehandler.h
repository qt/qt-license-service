/* Copyright (C) 2022 The Qt Company Ltd.
 *
 * SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0
*/
#pragma once

#include "clienthandler.h"
#include "utils.h"

namespace QLicenseService {

class SquishIdeHandler : public ClientHandler {

    public:
        SquishIdeHandler(const RequestInfo &request, const LicdSetup &settings)
                : ClientHandler(request, settings)
        {
            m_updateInterval = utils::strToInt(m_settings.get("squish_report_interval"));
            m_request.operation = OP_ADD_RESERVATION;
            m_floatingLicense = true;
            std::cout << "Client: Squish IDE";
        }

        bool isLicenseRequestDue() override { return true; }
        bool isCachedReservationValid(std::string &reply) override {return true;}
        void buildRequestJson() override {return;}
        int parseAndSaveResponse(std::string &response) override { return 0; }
        void prepareRelease() override
        {
            // TODO
            return;
        };
};

} // namespace QLicenseService
