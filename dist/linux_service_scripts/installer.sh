#!/usr/bin/env bash
# Copyright (C) 2022 The Qt Company Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0

name=qtlicd
installdir=/opt/${name}

function exit_fail {
    echo "ERROR! $1: Aborting installation"
    exit 1
}

function create_install_dir {
    umask u=rwx,go=rx;mkdir -p -m 755 ${installdir}
    if [ $? != 0 ]; then
        exit_fail "Could not create directory ${installdir}"
    fi

}

function copy_files {
    # Copy executables and .ini
    cp ${name} ${name}.ini qtlicensetool mocwrapper licheck ${installdir}/
    if [ $? != 0 ]; then
        exit_fail "Failed to copy files to $installdir"
    fi

    # Copy .service
    cp ${name}.service /etc/systemd/system/
    if [ $? != 0 ]; then
        exit_fail "Failed to copy $name.service to /etc/systemd/system/"
    fi

    # Create a symlink for qtlicensetool binary
    ln -s ${installdir}/qtlicensetool /usr/bin/qtlicensetool

    sync
}

function stop_daemon {
    running=true

    retries=0
    while [ $running != "false" ]
    do
        running=false
        systemctl is-active --quiet $name && running=true;
        if [ $running != "false" ]
        then
        systemctl stop $name
            sleep 2
            retries=$((retries+1))
            if [ $retries -gt 2 ]
            then
                exit_fail "Tried to stop qtlicenser service for $retries times already"
            fi
        fi
    done
    echo "Stopped $name.service"
}

function enable_and_start_daemon {
    # Enable the daemon
    systemctl daemon-reload
    systemctl enable ${name}.service

    # Start the daemon
    systemctl start ${name}.service
    if [ $? != 0 ]; then
        exit_fail "Could not start the daemon"
    fi
}

stop_daemon

create_install_dir

copy_files

enable_and_start_daemon

echo "Installation ok"
